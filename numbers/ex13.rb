#13. Дано натуральное число меньшее 256. Необходимо найти сумму всех нечетных битов этого числа.

x = 12
puts x.to_s(2).split(//).reverse.select.with_index{|e, i| e if i.odd?}.map(&:to_i).reduce(:+)
