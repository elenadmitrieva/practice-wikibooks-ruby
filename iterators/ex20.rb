#20. Дан массив чисел. Необходимо проверить, чередуются ли в нем целые и вещественные числа.

x = [5, 4.3, 3, 1.5, 6, 4.2, -1]
p x.each_cons(2).all?{|first, last| ((first.is_a?(Integer) and last.is_a?(Float)) or (first.is_a?(Float) and last.is_a?(Integer)))}
